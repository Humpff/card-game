const css = require('./app.scss'); // SCSS file which will be loaded as a separate css file on the index.html page
import StartGame from './modules/startGame';

const newGame = new StartGame();

var inputPlayerCount = 2; //hardcoded this for now
let startBtn = document.getElementsByClassName('initGame');

// Initalise the playerCounter
startBtn[0].addEventListener('click', function() {
	newGame.playerCount(inputPlayerCount);
});
