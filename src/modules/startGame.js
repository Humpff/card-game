// Initialise the game the moment a player clicks on "start game".
// The game can be only be reset when 1 player is left or when you click on "reset game"
export default class StartGame {

	constructor() {
		this.players = 2;
		this.amountOfCards = 52
		this.minimumCardsPerPlayer = 7;
		this.suits = ['C', 'D', 'H', 'S'];
		this.suitChars = ['♣', '♦', '♥', '♠'];
		this.suitNames = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];
		this.ranks = ['A', '2', '3', '4', '5', '6', '7','8', '9', 'T', 'J', 'Q', 'K'];
		this.rankNames = ['Ace', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven','Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King'];
	}

	playerCount( playerCounter ) {

		// if players are not set
		if( playerCounter < this.players ) {
			alert("You can't play this guy by yourself...");
			return false
		}

		this.divideCardsbyPlayers(playerCounter);
	}

	buildTheCards() {
		let i;
		let suitsObjIndex = {};
		let ranksObjIndex = {};

		this.suits.concat(this.suitChars, this.suitNames).forEach(function (e, i, a) {
				suitsObjIndex[e.toLowerCase()] = i;
		});

		this.ranks.concat(this.rankNames).forEach(function (e, i, a) {
				ranksObjIndex[e.toLowerCase()] = i;
		});

		console.log(suitsObjIndex);
		console.log(ranksObjIndex);
	}

	// If the minimum amount of players are met, then divided the cards up
	divideCardsbyPlayers( playerCounter ) {
		let amountOfCards = this.amountOfCards;
		let minimumCardsPerPlayer = this.minimumCardsPerPlayer;
		console.log(this.buildTheCards());

		// Each player should receive 7 cards
		// Remove the cards from the deck
	}

}
