# Kaartspel Pesten

## Rules
- Amount of players playing this game should be set by a variable (minimum of 2 players).
- All Players start with a similar amount of cards (in this case 7 cards for each player).
- A player should place a card on top of a stack. This card should be similar in either shape (hearts) or color.
- If a player is not possible to place a card on top of this stack, he/she should pull a new card.
- The first player to finish his/her deck of cards wins the game.

### Special cards
If time build the "special cards". See here the overview of all the special cards;

## Requirements
- Should be able to build a deck of cards.
- Each card should hold a value/rank.
- Each player should be able to draw a new card, adding to his/her current deck of cards.
- All this information needs to be stored somewhere (localStorage maybe?).
- You lose the game when you're the last player with at least 1 card in your hand.

## Installation instructions
- git clone git@gitlab.com:Humpff/card-game.git
- npm i
- npm run watch (this will create a local server: localhost:xxxx)
