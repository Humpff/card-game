const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // Plugin which can be used to create html templates
const ExtractTextPlugin = require('extract-text-webpack-plugin'); // If this is not included, the css is loaded in app.js. This plugin moves the css to a style on the index.html

const path = require('path');


module.exports = {
	entry: ['babel-polyfill', './src/index.js'],

	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env']
					}
				},
				include: [
					path.resolve(__dirname, 'src')
				],
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({ fallback: 'style-loader' , use: ['css-loader', 'sass-loader'] })
			}
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			title: 'Pest Me App',
			hash: true,
			filename: 'index.html',
			template: __dirname + '/index.ejs'
		}),

		new webpack.HotModuleReplacementPlugin(),

		new ExtractTextPlugin({
			filename: 'app.css'
		}),

		new webpack.optimize.UglifyJsPlugin({
		 include: /\.min\.js$/,
		 minimize: true
	 })
	],

	devServer: {
		hot: true,
		inline: true,
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		watchContentBase: true,
		publicPath: '/'
	},

	devtool: 'source-map',

	output: {
		filename: 'app.min.js',
		path: path.resolve(__dirname, 'dist')
	}
}
